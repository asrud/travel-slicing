/**
 * Constants
 */
 const FONT_FAMILY_PLAYFAIR = 'Playfair Display'
 const FONT_FAMILY_INTER = 'Inter'

/**
 * Global Styles Plugin
 *
 * This plugin modifies Tailwind’s base styles using values from the theme.
 * https://tailwindcss.com/docs/adding-base-styles#using-a-plugin
 */
 const globalStyles = ({ addBase, config }) => {
  addBase({
    'body': {
      color: config('theme.colors.dark'),
    },
    'a': {
      transition: 'color 0.2s',
      color: config('theme.colors.dark'),
      '&:hover': {
        color: config('theme.colors.primary'),
      },
    },
  });
}

/**
 * Container Plugin - modifies Tailwind’s default container.
 */
const containerBase = {
  maxWidth: '100%',
  marginLeft: 'auto',
  marginRight: 'auto',
  paddingLeft: '1.25rem', // 20px
  paddingRight: '1.25rem',
  '@screen md': {
    paddingLeft: '2.5rem', // 50px
    paddingRight: '2.5rem',
  },
  '@screen xl': {
    paddingLeft: '15rem', // 100px
    paddingRight: '15rem',
  },
}

const containerStyles = ({ addComponents }) => {
  addComponents({
    '.container': {
      ...containerBase,
      '@screen xl': {
        maxWidth: '1440px',
        paddingLeft: '15rem', // 100px
        paddingRight: '15rem',
      },
    },
    '.container-large': {
      ...containerBase,
      '@screen lg': {
        paddingLeft: '11.875rem',
        paddingRight: '11.875rem',
      },
    },
  })
}

const screensConfig = {
  'sm': '375px',
  'md': '768px',
  'lg': '1024px',
  'xl': '1280px',
  '2xl': '1440px',
  '3xl': '1900px',
  '4xl': '2500px',
}

module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: screensConfig,
    extend: {
      fontFamily: {
        heading: [FONT_FAMILY_PLAYFAIR, 'sans-serif'],
        paragraph : [FONT_FAMILY_INTER, 'sans-serif'],
      },
      fontSize: {
        '2xlp': '1.25rem',
        '3xlp': '2rem',
        '4xlp': '2.5rem',
        '5xlp': '3.5rem',
        '6xlp': ['4rem', '5rem'],
      },
      borderRadius: {
        'lgp': '0.625rem',
        '36': '2.25rem',  //36px
      },
      spacing: {
        '15': '3.75rem',
        '18': '4.375rem',
        '30': '7.5rem',
      },
      width: {
        'icon': '3rem', //48px
      },
      height: {
        'icon': '3rem', //48px
      },
      colors: {
        primary: '#FB8F1D',
        secondary: '#1ABE84',
        dark: '#202336',
        black: '#231F20',
        gray: '#B8BECD',
        semiblack: '#656263',
        brokenwhite: '#F6F6F6',
        gray2: '#F9F9FB',
      },
      minHeight: {
        '50vh': '50vh',
        '70vh': '70vh',
        '90vh': '90vh',
        'gallery-1': '320px',
        'gallery-2': '540px',
      },
    },
    // minWidth: {
    //   'card' : '120px'
    // }
  },
  variants: {
    extend: {},
  },
  corePlugins: {
    container: false,
  },
  plugins: [
    globalStyles,
    containerStyles,

    ({ addComponents }) => {
      const fixedCentered = {
        '.fixed-center': {
          position: 'fixed',
          left: '50%',
          top: '50%',
          transform: 'translate(-50%, -50%)'
        },
      }

      const dropShadowButton = {
        '.shadow-button': {
          filter: 'drop-shadow(0px 10px 20px rgba(0, 0, 0, 0.16))'
        },
      }

      const shapeElement = {
        '.shape-bottom': {
          clipPath: 'ellipse(95% 50% at 50% 40%)'
        },
        '.shape-left': {
          clipPath: 'circle(70% at 70% 50%)'
        },
        '.shape-top': {
          clipPath: 'circle(71.1% at 50% 73%)'
        },
      }

      addComponents(fixedCentered)
      addComponents(dropShadowButton)
      addComponents(shapeElement)
    },
  ],
}