import Head from 'next/head'
import Image from 'next/image'

export default function Home() {

  const star = () => {
    for (let index = 0; index < 5; index++) {
      return (
        <>
          <div className="mr-4">
            <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0)">
                <path d="M12.5854 20.0488L18.5854 23.7073C19.7561 24.439 21.0732 23.4146 20.7805 22.0976L19.1707 15.2195L24.5854 10.5366C25.6098 9.65854 25.0244 8.04878 23.7073 7.90244L16.6829 7.31707L13.9024 0.878049C13.3171 -0.292683 11.7073 -0.292683 11.1219 0.878049L8.34146 7.46341L1.31707 8.04878C-3.16277e-06 8.04878 -0.439028 9.65854 0.439021 10.5366L5.85366 15.2195L4.2439 22.0976C3.95122 23.4146 5.26829 24.2927 6.43902 23.7073L12.5854 20.0488Z" fill="#FFBB0C"/>
              </g>
              <defs>
                <clipPath id="clip0">
                <rect width="25.0244" height="24" fill="white"/>
                </clipPath>
              </defs>
            </svg>
          </div>
          <div className="mr-4">
            <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0)">
                <path d="M12.5854 20.0488L18.5854 23.7073C19.7561 24.439 21.0732 23.4146 20.7805 22.0976L19.1707 15.2195L24.5854 10.5366C25.6098 9.65854 25.0244 8.04878 23.7073 7.90244L16.6829 7.31707L13.9024 0.878049C13.3171 -0.292683 11.7073 -0.292683 11.1219 0.878049L8.34146 7.46341L1.31707 8.04878C-3.16277e-06 8.04878 -0.439028 9.65854 0.439021 10.5366L5.85366 15.2195L4.2439 22.0976C3.95122 23.4146 5.26829 24.2927 6.43902 23.7073L12.5854 20.0488Z" fill="#FFBB0C"/>
              </g>
              <defs>
                <clipPath id="clip0">
                <rect width="25.0244" height="24" fill="white"/>
                </clipPath>
              </defs>
            </svg>
          </div>
          <div className="mr-4">
            <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0)">
                <path d="M12.5854 20.0488L18.5854 23.7073C19.7561 24.439 21.0732 23.4146 20.7805 22.0976L19.1707 15.2195L24.5854 10.5366C25.6098 9.65854 25.0244 8.04878 23.7073 7.90244L16.6829 7.31707L13.9024 0.878049C13.3171 -0.292683 11.7073 -0.292683 11.1219 0.878049L8.34146 7.46341L1.31707 8.04878C-3.16277e-06 8.04878 -0.439028 9.65854 0.439021 10.5366L5.85366 15.2195L4.2439 22.0976C3.95122 23.4146 5.26829 24.2927 6.43902 23.7073L12.5854 20.0488Z" fill="#FFBB0C"/>
              </g>
              <defs>
                <clipPath id="clip0">
                <rect width="25.0244" height="24" fill="white"/>
                </clipPath>
              </defs>
            </svg>
          </div>
          <div className="mr-4">
            <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0)">
                <path d="M12.5854 20.0488L18.5854 23.7073C19.7561 24.439 21.0732 23.4146 20.7805 22.0976L19.1707 15.2195L24.5854 10.5366C25.6098 9.65854 25.0244 8.04878 23.7073 7.90244L16.6829 7.31707L13.9024 0.878049C13.3171 -0.292683 11.7073 -0.292683 11.1219 0.878049L8.34146 7.46341L1.31707 8.04878C-3.16277e-06 8.04878 -0.439028 9.65854 0.439021 10.5366L5.85366 15.2195L4.2439 22.0976C3.95122 23.4146 5.26829 24.2927 6.43902 23.7073L12.5854 20.0488Z" fill="#FFBB0C"/>
              </g>
              <defs>
                <clipPath id="clip0">
                <rect width="25.0244" height="24" fill="white"/>
                </clipPath>
              </defs>
            </svg>
          </div>
          <div className="mr-4">
            <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0)">
                <path d="M12.5854 20.0488L18.5854 23.7073C19.7561 24.439 21.0732 23.4146 20.7805 22.0976L19.1707 15.2195L24.5854 10.5366C25.6098 9.65854 25.0244 8.04878 23.7073 7.90244L16.6829 7.31707L13.9024 0.878049C13.3171 -0.292683 11.7073 -0.292683 11.1219 0.878049L8.34146 7.46341L1.31707 8.04878C-3.16277e-06 8.04878 -0.439028 9.65854 0.439021 10.5366L5.85366 15.2195L4.2439 22.0976C3.95122 23.4146 5.26829 24.2927 6.43902 23.7073L12.5854 20.0488Z" fill="#FFBB0C"/>
              </g>
              <defs>
                <clipPath id="clip0">
                <rect width="25.0244" height="24" fill="white"/>
                </clipPath>
              </defs>
            </svg>
          </div>
        </>
      )
    } 
  }
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600&family=Playfair+Display:wght@500;600;700&display=swap" rel="stylesheet" /> 
      </Head>

      <main>
        <div className="container">
          <div className="wrapper">
            {/* header */}
            <div className="flex flex-wrap justify-between items-center pt-9">
              <div className="logo">
                <svg width="41" height="41" viewBox="0 0 41 41" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <circle cx="20.5" cy="20.5" r="20.5" fill="#1ABE84"/>
                  <circle cx="20.5" cy="20.5" r="9" fill="#FB8F1D" stroke="white" stroke-width="3"/>
                </svg>
              </div>
              <div className="menu hidden lg:flex flex-wrap space-x-9 items-center">
                <nav>
                  <ul className="flex flex-wrap space-x-9 font-paragraph"> 
                    <li>
                      <p className="text-black">Home</p>
                    </li>
                    <li>
                      <p className="text-gray">Destination</p>
                    </li>
                    <li>
                      <p className="text-gray">About</p>
                    </li>
                    <li>
                      <p className="text-gray">Partner</p>
                    </li>
                  </ul>
                </nav>
                <div className="flex flex-wrap space-x-9">
                  <button className="px-11 py-2.5 border rounded-lg border-primary font-paragraph text-primary">Login</button>
                  <button className="px-11 py-2.5 border rounded-lg bg-primary font-paragraph text-white">Register</button>
                </div>
              </div>
              <div className="menu-mobile lg:hidden"> 
                <svg viewBox="0 0 100 80" width="40" height="40" fill="#FB8F1D"> 
                  <rect width="100" height="10"></rect>
                  <rect y="30" width="100" height="10"></rect>
                  <rect y="60" width="100" height="10"></rect>
                </svg>
              </div>
            </div>
            {/* ATF */}
            <div className="grid grid-cols-12 gap-6 pt-18">
              <div className="col-span-6 relative">
                <div className="absolute top-15 right-0">
                  <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.698 20.5325L13.6228 14.0237C13.6555 13.7748 13.8545 13.5478 14.1149 13.4976L20.6387 13.4266C20.8882 13.4594 21.0872 13.2323 21.1308 12.9004L21.1007 9.26722C21.1335 9.01832 20.9058 8.81959 20.5731 8.77587L14.0493 8.84688C13.7998 8.81408 13.5721 8.61536 13.5217 8.35553L13.4464 1.84671C13.4792 1.59781 13.2515 1.39908 12.9188 1.35536L9.2772 1.38317C9.0277 1.35038 8.82866 1.57742 8.78504 1.9093L8.86028 8.41811C8.82756 8.66701 8.62852 8.89406 8.36811 8.94423L1.84431 9.01526C1.59481 8.98247 1.39577 9.20951 1.35215 9.54138L1.38225 13.1746C1.34953 13.4235 1.57723 13.6222 1.9099 13.666L8.4337 13.5949C8.6832 13.6277 8.9109 13.8265 8.96136 14.0863L9.03654 20.5951C9.00383 20.844 9.23153 21.0427 9.5642 21.0865L13.2058 21.0587C13.4662 21.0085 13.6652 20.7814 13.698 20.5325Z" fill="#FDD08D"/>
                  </svg>
                </div>
                <div className="absolute bottom-1/3 -left-20">
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                    <defs>
                    <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#BFF0DD"/>
                    <stop offset="1" stop-color="#57D2A3"/>
                    </linearGradient>
                    </defs>
                  </svg>
                </div>
                <h1 className="font-heading text-6xlp font-bold mb-18">Explore and Travel</h1>
                <div>
                  <p className="font-paragraph text-3xlp font-semibold mb-5">Holiday Finder</p>
                  <div className="bg-black w-8 h-1 mb-12"></div>
                  <p className="font-paragraph text-xl text-gray mb-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                  <button className="px-11 py-2.5 border rounded-lg bg-primary font-paragraph text-white">Explore</button>
                </div>
                <div className="absolute -bottom-7 left-2/3">
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                    <defs>
                    <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#BFF0DD"/>
                    <stop offset="1" stop-color="#57D2A3"/>
                    </linearGradient>
                    </defs>
                  </svg>
                </div>
              </div>
              <div className="col-span-6 relative">
                <div>
                  <Image 
                    src="/atf.png" 
                    alt="atf-image"
                    width={687}
                    height={654}
                  />
                </div>
              </div>
            </div>
            {/* Explore */}
            <div className="grid grid-cols-12 gap-6 pt-64">
              <div className="col-span-6 relative">
                <Image
                  src="/explore.png"
                  alt="explore-image"
                  width={659}
                  height={534}
                 />
                 <div className="absolute -top-15 -right-7">
                    <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.698 20.5325L13.6228 14.0237C13.6555 13.7748 13.8545 13.5478 14.1149 13.4976L20.6387 13.4266C20.8882 13.4594 21.0872 13.2323 21.1308 12.9004L21.1007 9.26722C21.1335 9.01832 20.9058 8.81959 20.5731 8.77587L14.0493 8.84688C13.7998 8.81408 13.5721 8.61536 13.5217 8.35553L13.4464 1.84671C13.4792 1.59781 13.2515 1.39908 12.9188 1.35536L9.2772 1.38317C9.0277 1.35038 8.82866 1.57742 8.78504 1.9093L8.86028 8.41811C8.82756 8.66701 8.62852 8.89406 8.36811 8.94423L1.84431 9.01526C1.59481 8.98247 1.39577 9.20951 1.35215 9.54138L1.38225 13.1746C1.34953 13.4235 1.57723 13.6222 1.9099 13.666L8.4337 13.5949C8.6832 13.6277 8.9109 13.8265 8.96136 14.0863L9.03654 20.5951C9.00383 20.844 9.23153 21.0427 9.5642 21.0865L13.2058 21.0587C13.4662 21.0085 13.6652 20.7814 13.698 20.5325Z" fill="#FDD08D"/>
                    </svg>
                  </div>
              </div>
              <div className="col-span-6 ml-8 relative">
                <div>
                  <h1 className="text-black text-4xl font-bold font-heading">A new way to explore the world </h1>
                  <p className="text-gray text-base pt-5 font-paragraph">
                    For decades travellers have reached for Lonely Planet books when looking to plan and execute their perfect 
                    trip, but now, they can also let Lonely Planet Experiences lead the way
                  </p>
                  <button className="bg-primary px-9 py-2 rounded-lg font-paragraph text-white mt-10">Learn More</button>
                </div>
                <div className="absolute bottom-1/3 right-18">
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                    <defs>
                    <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#BFF0DD"/>
                    <stop offset="1" stop-color="#57D2A3"/>
                    </linearGradient>
                    </defs>
                  </svg>
                </div>
              </div>
            </div>
            {/* Feature */}
            <div>
              <div className="grid grid-cols-12 gap-6 pt-72">
                <div className="col-span-10">
                    <h1 className="text-black text-4xl font-bold font-heading">Featured destinations</h1>
                </div>
                <div className="col-span-2 flex flex-wrap justify-end items-center space-x-3">
                  <span className="text-primary font-paragraph text-base text-right">View all</span>
                  <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 1L5 5.5L1 10" stroke="#FB8F1D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </div>
              </div>
              <div className="grid grid-cols-12 gap-6 relative">
                  <div className="absolute -left-20 bottom-1/3">
                    <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M13.698 20.5325L13.6228 14.0237C13.6555 13.7748 13.8545 13.5478 14.1149 13.4976L20.6387 13.4266C20.8882 13.4594 21.0872 13.2323 21.1308 12.9004L21.1007 9.26722C21.1335 9.01832 20.9058 8.81959 20.5731 8.77587L14.0493 8.84688C13.7998 8.81408 13.5721 8.61536 13.5217 8.35553L13.4464 1.84671C13.4792 1.59781 13.2515 1.39908 12.9188 1.35536L9.2772 1.38317C9.0277 1.35038 8.82866 1.57742 8.78504 1.9093L8.86028 8.41811C8.82756 8.66701 8.62852 8.89406 8.36811 8.94423L1.84431 9.01526C1.59481 8.98247 1.39577 9.20951 1.35215 9.54138L1.38225 13.1746C1.34953 13.4235 1.57723 13.6222 1.9099 13.666L8.4337 13.5949C8.6832 13.6277 8.9109 13.8265 8.96136 14.0863L9.03654 20.5951C9.00383 20.844 9.23153 21.0427 9.5642 21.0865L13.2058 21.0587C13.4662 21.0085 13.6652 20.7814 13.698 20.5325Z" fill="#FDD08D"/>
                    </svg>
                  </div>
                  <div className="col-span-3 pt-14 rounded-lg relative">
                      <Image
                        className="rounded-lg"
                        src="/Rajaampat.png"
                        alt="rajaampat-image"
                        width={261}
                        height={337}
                      />
                      <div className="absolute bg-white h-14 w-30 rounded-lg rounded-tl-none bottom-0 left-0 pt-2">
                        <h2 className="text-black text-base font-semibold font-paragraph">Raja Ampat</h2>
                        <p className="text-gray font-paragraph text-sm ">Indonesia</p>
                      </div>
                  </div>
                  <div className="col-span-3 pt-14 rounded-lg relative">
                      <Image
                        className="rounded-lg"
                        src="/Fanjingshan.png"
                        alt="Fanjingshan-image"
                        width={261}
                        height={337}
                      />
                      <div className="absolute bg-white h-14 w-30 rounded-lg rounded-tl-none bottom-0 left-0 pt-2">
                        <h2 className="text-black text-base font-semibold font-paragraph">Fanjingshan</h2>
                        <p className="text-gray font-paragraph text-sm ">China</p>
                      </div>
                  </div>
                  <div className="col-span-3 pt-14 rounded-lg relative">
                      <Image
                        className="rounded-lg"
                        src="/Vevey.png"
                        alt="Vevey-image"
                        width={261}
                        height={337}
                      />
                      <div className="absolute bg-white h-14 w-30 rounded-lg rounded-tl-none bottom-0 left-0 pt-2">
                        <h2 className="text-black text-base font-semibold font-paragraph">Vevey</h2>
                        <p className="text-gray font-paragraph text-sm ">Switzerland</p>
                      </div>
                  </div>
                  <div className="col-span-3 pt-14 rounded-lg relative">
                      <Image
                        className="rounded-lg"
                        src="/Skadar.png"
                        alt="Skadar-image" 
                        width={261}
                        height={337}
                      />
                      <div className="absolute bg-white h-14 w-30 rounded-lg rounded-tl-none bottom-0 left-0 pt-2">
                        <h2 className="text-black text-base font-semibold font-paragraph">Skadar</h2>
                        <p className="text-gray font-paragraph text-sm ">Montenegro</p>
                      </div>
                  </div>
                  <div className="absolute -bottom-16 right-2/3">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                      <defs>
                      <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                      <stop stop-color="#BFF0DD"/>
                      <stop offset="1" stop-color="#57D2A3"/>
                      </linearGradient>
                      </defs>
                    </svg>
                  </div>
              </div>
            </div>
            {/* Guides */}
            <div className="grid grid-cols-12 gap-6 pt-64 flex flex-wrap justify-between items-center">
              <div className="col-span-6 mr-10 relative">
                <div className="absolute -top-7 left-5">
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                    <defs>
                    <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#BFF0DD"/>
                    <stop offset="1" stop-color="#57D2A3"/>
                    </linearGradient>
                    </defs>
                  </svg>
                </div>
                <div>
                  <h1 className="text-black text-4xl font-bold font-heading">Guides by Thousand Sunny</h1>
                  <p className="text-gray text-base pt-5 font-paragraph">
                    Packed with tips and advice from our on-the-ground experts, our city guides app (iOS and Android) is the ultimate resource before and during a trip.
                  </p>
                  <button className="bg-primary px-9 py-2 rounded-lg font-paragraph text-white mt-10">Download</button>
                </div>
                <div className="absolute right-0 bottom-3 rotate-12">
                  <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.698 20.5325L13.6228 14.0237C13.6555 13.7748 13.8545 13.5478 14.1149 13.4976L20.6387 13.4266C20.8882 13.4594 21.0872 13.2323 21.1308 12.9004L21.1007 9.26722C21.1335 9.01832 20.9058 8.81959 20.5731 8.77587L14.0493 8.84688C13.7998 8.81408 13.5721 8.61536 13.5217 8.35553L13.4464 1.84671C13.4792 1.59781 13.2515 1.39908 12.9188 1.35536L9.2772 1.38317C9.0277 1.35038 8.82866 1.57742 8.78504 1.9093L8.86028 8.41811C8.82756 8.66701 8.62852 8.89406 8.36811 8.94423L1.84431 9.01526C1.59481 8.98247 1.39577 9.20951 1.35215 9.54138L1.38225 13.1746C1.34953 13.4235 1.57723 13.6222 1.9099 13.666L8.4337 13.5949C8.6832 13.6277 8.9109 13.8265 8.96136 14.0863L9.03654 20.5951C9.00383 20.844 9.23153 21.0427 9.5642 21.0865L13.2058 21.0587C13.4662 21.0085 13.6652 20.7814 13.698 20.5325Z" fill="#FDD08D"/>
                  </svg>
                </div>
              </div>
              <div className="col-span-6 relative">
                <Image
                  src="/Thousand.png"
                  alt="Thousand-image"
                  width={599}
                  height={542}
                 />
                <div className="absolute -right-20 top-4">
                  <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.698 20.5325L13.6228 14.0237C13.6555 13.7748 13.8545 13.5478 14.1149 13.4976L20.6387 13.4266C20.8882 13.4594 21.0872 13.2323 21.1308 12.9004L21.1007 9.26722C21.1335 9.01832 20.9058 8.81959 20.5731 8.77587L14.0493 8.84688C13.7998 8.81408 13.5721 8.61536 13.5217 8.35553L13.4464 1.84671C13.4792 1.59781 13.2515 1.39908 12.9188 1.35536L9.2772 1.38317C9.0277 1.35038 8.82866 1.57742 8.78504 1.9093L8.86028 8.41811C8.82756 8.66701 8.62852 8.89406 8.36811 8.94423L1.84431 9.01526C1.59481 8.98247 1.39577 9.20951 1.35215 9.54138L1.38225 13.1746C1.34953 13.4235 1.57723 13.6222 1.9099 13.666L8.4337 13.5949C8.6832 13.6277 8.9109 13.8265 8.96136 14.0863L9.03654 20.5951C9.00383 20.844 9.23153 21.0427 9.5642 21.0865L13.2058 21.0587C13.4662 21.0085 13.6652 20.7814 13.698 20.5325Z" fill="#FDD08D"/>
                  </svg>
                </div>
              </div>
            </div>
            {/* Testimonials */}
            <div className="grid grid-cols-12 gap-6 pt-64 flex flex-wrap justify-between items-center">
              <div className="col-span-12">
                <h1 className="text-black text-4xl font-bold font-heading">Testimonials</h1>
              </div>
              <div className="col-span-5 pr-10 relative">
                <div className="absolute bottom-30 -left-15 z-20">
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                    <defs>
                    <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#BFF0DD"/>
                    <stop offset="1" stop-color="#57D2A3"/>
                    </linearGradient>
                    </defs>
                  </svg>
                </div>
                <div>
                  <div className="flex flex-wrap">
                    { 
                      star() 
                    }
                  </div>
                  <p className="text-black text-2xl pt-5 font-paragraph">
                    “Quisque in lacus a urna fermentum euismod. Integer mi nibh, dapibus ac scelerisque eu, facilisis quis purus. Morbi blandit sit amet turpis nec”
                  </p>
                  <h2 className="text-black mt-10 font-paragraph font-bold text-2xl">Edward Newgate</h2>
                  <p className="text-black text-lg font-paragraph">Founder Circle</p>
                </div>
              </div>
              <div className="col-span-7 flex flex-wrap justify-center">
                  <div className="col-span-3 relative">
                    <Image className="rounded-xl z-10"
                      src="/photo-testi.png"
                      alt="photo-testi-image"
                      width={384}
                      height={491}
                    />
                    <div className="absolute -top-4 -right-4 z-0">
                      <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.91229 29.7239C-2.94163 19.2289 1.78109 6.76622 12.2761 1.91229C22.771 -2.94163 35.2338 1.78106 40.0877 12.276C44.9416 22.771 40.2189 35.2338 29.7239 40.0877C19.229 44.9416 6.76621 40.2189 1.91229 29.7239ZM7.68451 27.1002C11.0954 34.4466 19.7537 37.7263 27.2314 34.3154C34.5779 30.9046 37.8575 22.2463 34.4467 14.7686C31.0358 7.29095 22.3775 4.14246 14.8998 7.55333C7.55332 10.9642 4.27365 19.7537 7.68451 27.1002Z" fill="url(#paint0_linear)"/>
                        <defs>
                        <linearGradient id="paint0_linear" x1="12.5652" y1="1.81889" x2="29.5502" y2="40.2278" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#BFF0DD"/>
                        <stop offset="1" stop-color="#57D2A3"/>
                        </linearGradient>
                        </defs>
                      </svg>
                    </div>
                    <div className="absolute -bottom-2 -left-4 z-20">
                      <svg width="37" height="36" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.817 32.8433L21.8216 22.5456C21.8781 22.1524 22.1972 21.7971 22.6101 21.7226L32.9314 21.7341C33.3255 21.7907 33.6447 21.4353 33.72 20.9111L33.7413 15.1631C33.7978 14.77 33.4413 14.4513 32.9159 14.3759L22.5946 14.3644C22.2005 14.3078 21.8441 13.9891 21.7693 13.5771L21.7738 3.27937C21.8302 2.88625 21.4738 2.56756 20.9484 2.49208L15.1872 2.46698C14.7931 2.41037 14.474 2.76575 14.3987 3.28992L14.3942 13.5877C14.3377 13.9808 14.0185 14.3362 13.6056 14.4106L3.28426 14.3991C2.8902 14.3425 2.57102 14.6979 2.49573 15.2221L2.47439 20.9701C2.41792 21.3632 2.77435 21.6819 3.29977 21.7574L13.6211 21.7688C14.0152 21.8254 14.3716 22.1441 14.4465 22.5561L14.4419 32.8538C14.3855 33.247 14.7419 33.5656 15.2673 33.6411L21.0285 33.6662C21.4414 33.5918 21.7605 33.2364 21.817 32.8433Z" fill="#FDD08D"/>
                      </svg>
                    </div>
                    <div className="absolute bg-white h-15 w-32 rounded-lg rounded-tr-none bottom-0 right-0 pt-2 z-20">
                      <div className="flex flex-wrap justify-end space-x-6">
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M22 25L18 20.5L22 16" stroke="#D7D7D7" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                          <circle cx="20" cy="20" r="19" transform="rotate(-180 20 20)" stroke="#D7D7D7" stroke-width="2"/>
                        </svg>
                        <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M18 15L22 19.5L18 24" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                          <circle cx="20" cy="20" r="19" stroke="black" stroke-width="2"/>
                        </svg>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            {/* Trending */}
            <div>
              <div className="grid grid-cols-12 gap-6 pt-72">
                <div className="col-span-10 relative">
                    <div className="absolute -bottom-80 -left-15 z-20">
                      <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                        <defs>
                        <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#BFF0DD"/>
                        <stop offset="1" stop-color="#57D2A3"/>
                        </linearGradient>
                        </defs>
                      </svg>
                    </div>
                    <div className="absolute -top-9 left-2/4 z-20">
                      <svg width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                       <path d="M13.698 20.5325L13.6228 14.0237C13.6555 13.7748 13.8545 13.5478 14.1149 13.4976L20.6387 13.4266C20.8882 13.4594 21.0872 13.2323 21.1308 12.9004L21.1007 9.26722C21.1335 9.01832 20.9058 8.81959 20.5731 8.77587L14.0493 8.84688C13.7998 8.81408 13.5721 8.61536 13.5217 8.35553L13.4464 1.84671C13.4792 1.59781 13.2515 1.39908 12.9188 1.35536L9.2772 1.38317C9.0277 1.35038 8.82866 1.57742 8.78504 1.9093L8.86028 8.41811C8.82756 8.66701 8.62852 8.89406 8.36811 8.94423L1.84431 9.01526C1.59481 8.98247 1.39577 9.20951 1.35215 9.54138L1.38225 13.1746C1.34953 13.4235 1.57723 13.6222 1.9099 13.666L8.4337 13.5949C8.6832 13.6277 8.9109 13.8265 8.96136 14.0863L9.03654 20.5951C9.00383 20.844 9.23153 21.0427 9.5642 21.0865L13.2058 21.0587C13.4662 21.0085 13.6652 20.7814 13.698 20.5325Z" fill="#FDD08D"/>
                      </svg>
                    </div>
                    <h1 className="text-black text-4xl font-bold font-heading">Trending stories</h1>
                </div>
                <div className="col-span-2 flex flex-wrap justify-end items-center space-x-3">
                  <span className="text-primary font-paragraph text-base text-right">View all</span>
                  <svg width="6" height="11" viewBox="0 0 6 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 1L5 5.5L1 10" stroke="#FB8F1D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  </svg>
                </div>
              </div>
              <div className="grid grid-cols-12 gap-6 relative">
                  <div className="col-span-3 pt-14 rounded-lg">
                      <Image
                        className="rounded-lg rounded-bl-none rounded-br-none"
                        src="/Meditation.png"
                        alt="Meditation-image"
                        width={261}
                        height={232}
                      />
                      <div className="rounded-lg rounded-tl-none pt-2">
                        <h2 className="text-black text-base font-bold font-paragraph">The many benefits of taking a healing holiday</h2>
                        <p className="text-gray font-paragraph text-xs pt-4">‘Helaing holidays’ are on the rise tohelp maximise your health and happines...</p>
                        <button className="pt-3 text-primary font-paragraph font-semibold text-xs">Read more</button>
                      </div>
                  </div>
                  <div className="col-span-3 pt-14 rounded-lg">
                      <Image
                        className="rounded-lg rounded-bl-none rounded-br-none"
                        src="/food.png"
                        alt="food-image"
                        width={261}
                        height={232}
                      />
                      <div className="rounded-lg rounded-tl-none pt-2">
                        <h2 className="text-black text-base font-bold font-paragraph">The best Kyoto restaurant to try Japanese food</h2>
                        <p className="text-gray font-paragraph text-xs pt-4">From tofu to teahouses, here’s our guide to Kyoto’s best restaurants to visit...</p>
                        <button className="pt-3 text-primary font-paragraph font-semibold text-xs">Read more</button>
                      </div>
                  </div>
                  <div className="col-span-3 pt-14 rounded-lg">
                      <Image
                        className="rounded-lg rounded-bl-none rounded-br-none"
                        src="/pyramid.png"
                        alt="pyramid-image"
                        width={261}
                        height={232}
                      />
                      <div className="rounded-lg rounded-tl-none pt-2">
                        <h2 className="text-black text-base font-bold font-paragraph">Skip Chichen Itza and head to this remote Yucatan</h2>
                        <p className="text-gray font-paragraph text-xs pt-4">It’s remote and challenging to get, but braving the jungle and exploring these ruins without the...</p>
                        <button className="pt-3 text-primary font-paragraph font-semibold text-xs">Read more</button>
                      </div>
                  </div>
                  <div className="col-span-3 pt-14 rounded-lg">
                      <Image
                        className="rounded-lg rounded-bl-none rounded-br-none"
                        src="/surf.png"
                        alt="surf-image" 
                        width={261}
                        height={232}
                      />
                      <div className="rounded-lg rounded-tl-none pt-2">
                        <h2 className="text-black text-base font-bold font-paragraph">Surf’s up at these beginner spots around the world</h2>
                        <p className="text-gray font-paragraph text-xs pt-4">If learning to surf has in on your to- do list for a while, the good news is: it’s never too late...</p>
                        <button className="pt-3 text-primary font-paragraph font-semibold text-xs">Read more</button>
                      </div>
                  </div>
                  <div className="absolute -bottom-20 left-2/3 z-20">
                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M0.908711 14.1542C-1.39785 9.15664 0.846368 3.22201 5.83353 0.910617C10.8207 -1.40078 16.743 0.848124 19.0495 5.84573C21.3561 10.8433 19.1119 16.778 14.1247 19.0894C9.13753 21.4008 3.21528 19.1518 0.908711 14.1542ZM3.65165 12.9048C5.27248 16.4032 9.3869 17.9649 12.9403 16.3407C16.4313 14.7165 17.9898 10.5935 16.3689 7.03267C14.7481 3.47188 10.6337 1.9726 7.08033 3.59682C3.58931 5.22104 2.03082 9.40652 3.65165 12.9048Z" fill="url(#paint0_linear)"/>
                      <defs>
                      <linearGradient id="paint0_linear" x1="5.97093" y1="0.866138" x2="14.0704" y2="19.1436" gradientUnits="userSpaceOnUse">
                      <stop stop-color="#BFF0DD"/>
                      <stop offset="1" stop-color="#57D2A3"/>
                      </linearGradient>
                      </defs>
                    </svg>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      <footer className="flex items-center justify-center w-full pt-18 pb-8 mt-72 bg-gray2">
        <div className=" flex flex-wrap justify-center container-large px-24">
          <div className="w-64 mr-24">
            <svg width="41" height="41" viewBox="0 0 41 41" fill="none" xmlns="http://www.w3.org/2000/svg">
              <circle cx="20.5" cy="20.5" r="20.5" fill="#1ABE84"/>
              <circle cx="20.5" cy="20.5" r="9" fill="#FB8F1D" stroke="white" stroke-width="3"/>
            </svg>
            <p className="mt-7 text-gray font-paragraph text-lg">
            Plan and book your perfect trip with 
            expert advice, travel tips destination
            information from us
            </p>
            <p className="mt-14 text-gray font-paragraph text-lg">
              ©2020 Thousand Sunny. All rights reserved
            </p>
          </div>
          <div className="w-64">
            <h2 className="text-black font-bold text-xl font-paragraph mb-3">Destinations</h2>
            <ul className="text-gray font-paragraph text-lg space-y-2">
              <li>Africa</li>
              <li>Antarctica</li>
              <li>Asia</li>
              <li>Europe</li>
              <li>America</li>
            </ul>
          </div>
          <div className="w-64">
            <h2 className="text-black font-bold text-xl font-paragraph mb-3">Shop</h2>
            <ul className="text-gray font-paragraph text-lg space-y-2">
              <li>Destination Guides</li>
              <li>Pictorial & Gifts</li>
              <li>Special Offers</li>
              <li>Delivery Times</li>
              <li>FAQs</li>
            </ul>
          </div>
          <div className="w-64">
            <h2 className="text-black font-bold text-xl font-paragraph mb-3">Interests</h2>
            <ul className="text-gray font-paragraph text-lg space-y-2">
              <li>Adventure Travel</li>
              <li>Art And Culture</li>
              <li>Wildlife And Nature</li>
              <li>Family Holidays</li>
              <li>Food And Drink</li>
            </ul>
          </div>
          <div className="border-t border-gray mt-20 w-full flex flex-wrap justify-center items-center space-x-5 pt-3">
            <svg width="25" height="20" viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0)">
              <path d="M24.999 2.3741C24.0253 2.78177 23.0273 3.04556 22.0536 3.16547C23.1733 2.51799 23.9036 1.58273 24.3174 0.383693C23.295 0.959233 22.1996 1.36691 21.0555 1.58273C20.0331 0.527578 18.7916 0 17.3067 0C15.8948 0 14.6777 0.503597 13.6797 1.48681C12.6816 2.47002 12.1704 3.66906 12.1704 5.05995C12.1704 5.44365 12.2191 5.82734 12.2921 6.21103C10.1986 6.11511 8.22688 5.58753 6.40117 4.65228C4.57546 3.71703 3.01753 2.47002 1.72736 0.935252C1.26485 1.72662 1.02142 2.56595 1.02142 3.47722C1.02142 4.34053 1.21617 5.15588 1.62999 5.8753C2.04382 6.6187 2.6037 7.21823 3.2853 7.67386C2.45765 7.64988 1.67868 7.43405 0.972738 7.02638V7.09832C0.972738 8.32134 1.36222 9.3765 2.14119 10.3118C2.92016 11.223 3.89387 11.8225 5.08666 12.0623C4.64849 12.1823 4.18598 12.2302 3.74781 12.2302C3.4557 12.2302 3.13924 12.2062 2.7741 12.1583C3.09056 13.1655 3.69913 14.0048 4.57546 14.6523C5.4518 15.2998 6.44986 15.6355 7.56962 15.6595C5.71957 17.0983 3.57741 17.8177 1.21617 17.8177C0.753654 17.8177 0.339827 17.7938 -0.0253143 17.7458C2.36028 19.2566 4.98929 20 7.83739 20C9.6631 20 11.3671 19.7122 12.9737 19.1607C14.5803 18.5851 15.9435 17.8417 17.0633 16.8825C18.1831 15.9233 19.1811 14.8201 19.9844 13.5971C20.8121 12.3501 21.4206 11.0552 21.8101 9.71223C22.1996 8.3693 22.4187 7.0024 22.4187 5.65947C22.4187 5.3717 22.4187 5.15588 22.3944 5.01199C23.4411 4.26858 24.2931 3.40528 24.999 2.3741Z" fill="black"/>
              </g>
              <defs>
              <clipPath id="clip0">
              <rect width="25" height="20" fill="white"/>
              </clipPath>
              </defs>
            </svg>
            <svg width="10" height="22" viewBox="0 0 10 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M6.64671 7.21043V5.3054C6.64671 5.01862 6.66667 4.7933 6.68663 4.64991C6.70659 4.48603 6.76647 4.34264 6.86627 4.19925C6.96607 4.05587 7.10579 3.95344 7.30539 3.89199C7.50499 3.83054 7.78443 3.81006 8.12375 3.81006H9.98004V0H7.00599C5.28942 0 4.07186 0.409683 3.33333 1.24953C2.59481 2.08939 2.21557 3.29795 2.21557 4.93669V7.21043H0V11H2.21557V22H6.64671V11H9.6008L10 7.21043H6.64671Z" fill="black"/>
            </svg>
            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M15.9305 0H6.06953C2.71746 0 0 2.71746 0 6.06953V15.9305C0 19.2825 2.71746 22 6.06953 22H15.9305C19.2825 22 22 19.2825 22 15.9305V6.06953C22 2.71746 19.2825 0 15.9305 0ZM20.0473 15.9305C20.0473 18.2086 18.1923 20.0473 15.9305 20.0473H6.06953C3.79142 20.0473 1.95266 18.1923 1.95266 15.9305V6.06953C1.95266 3.80769 3.80769 1.95266 6.06953 1.95266H15.9305C18.2086 1.95266 20.0473 3.80769 20.0473 6.06953V15.9305Z" fill="black"/>
              <path d="M11 5.33716C7.87573 5.33716 5.321 7.87562 5.321 10.9999C5.321 14.1241 7.85946 16.6789 11 16.6789C14.1243 16.6789 16.679 14.1404 16.679 10.9999C16.679 7.87562 14.1243 5.33716 11 5.33716ZM11 14.7262C8.94969 14.7262 7.27366 13.0664 7.27366 11.0162C7.28993 8.94958 8.94969 7.28982 11 7.28982C13.0503 7.28982 14.7263 8.94958 14.7263 10.9999C14.7263 13.0502 13.0503 14.7262 11 14.7262Z" fill="black"/>
              <path d="M16.9068 3.67749C16.5325 3.67749 16.1583 3.82394 15.8979 4.10057C15.6376 4.36092 15.4748 4.73518 15.4748 5.10944C15.4748 5.4837 15.6213 5.85796 15.8979 6.11832C16.1583 6.37867 16.5325 6.5414 16.9068 6.5414C17.2811 6.5414 17.6553 6.39495 17.9157 6.11832C18.176 5.85796 18.3388 5.4837 18.3388 5.10944C18.3388 4.73518 18.1923 4.36092 17.9157 4.10057C17.6553 3.82394 17.2973 3.67749 16.9068 3.67749Z" fill="black"/>
            </svg>
            <svg width="23" height="22" viewBox="0 0 23 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M5.21578 7.14941H0.275716V22H5.21578V7.14941Z" fill="black"/>
              <path d="M21.4376 8.50559C20.4036 7.37916 19.025 6.80444 17.3247 6.80444C16.7043 6.80444 16.1299 6.87341 15.6014 7.03433C15.0959 7.19525 14.6593 7.40214 14.2917 7.67801C13.947 7.95387 13.6483 8.20674 13.4416 8.45962C13.2348 8.6895 13.0509 8.94237 12.8671 9.24123V7.12628H7.95004L7.97302 7.83893C7.97302 8.32168 7.996 9.79295 7.996 12.2757C7.996 14.7585 7.996 17.9769 7.97302 21.9769H12.8901V13.701C12.8901 13.1952 12.9361 12.7815 13.0509 12.4826C13.2577 11.9769 13.5794 11.5401 13.993 11.1952C14.4066 10.8504 14.9351 10.6895 15.5784 10.6895C16.4286 10.6895 17.0719 10.9884 17.4625 11.5861C17.8761 12.1838 18.0599 13.0113 18.0599 14.0458V21.9769H22.977V13.4711C23 11.2872 22.4945 9.63203 21.4376 8.50559Z" fill="black"/>
              <path d="M2.78022 0C1.95305 0 1.28671 0.252874 0.781219 0.735632C0.252747 1.21839 0 1.81609 0 2.55172C0 3.28736 0.252747 3.88506 0.758242 4.39081C1.26374 4.87356 1.90709 5.12644 2.73427 5.12644H2.75724C3.60739 5.12644 4.27373 4.87356 4.77922 4.39081C5.28472 3.90805 5.53746 3.28736 5.53746 2.55172C5.53746 1.81609 5.26174 1.1954 4.77922 0.712644C4.27373 0.252874 3.60739 0 2.78022 0Z" fill="black"/>
            </svg>
            <svg width="29" height="20" viewBox="0 0 29 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clip-path="url(#clip0)">
              <path d="M27.9856 7.64988C27.9856 7.09832 27.9376 6.3789 27.8417 5.51559C27.7698 4.65228 27.6499 3.88489 27.506 3.21343C27.3381 2.44604 26.9784 1.82254 26.4269 1.29496C25.8753 0.767386 25.2278 0.479616 24.4844 0.383693C22.1823 0.119904 18.6811 0 14.0048 0C9.32854 0 5.82734 0.119904 3.52518 0.383693C2.78177 0.479616 2.13429 0.767386 1.58273 1.29496C1.03118 1.82254 0.671463 2.44604 0.503597 3.21343C0.335731 3.88489 0.239808 4.65228 0.143885 5.51559C0.0719424 6.3789 0.0239808 7.09832 0 7.64988C0 8.22542 0 8.99281 0 10C0 11.0072 0 11.7746 0.0239808 12.3501C0.0239808 12.9017 0.0719424 13.6211 0.167866 14.4844C0.215827 15.3477 0.335731 16.1151 0.479616 16.7866C0.647482 17.554 1.00719 18.1775 1.55875 18.705C2.11031 19.2326 2.75779 19.5204 3.5012 19.6163C5.80336 19.8801 9.30456 20 13.9808 20C18.6571 20 22.1583 19.8801 24.4604 19.6163C25.2038 19.5444 25.8513 19.2326 26.4029 18.705C26.9544 18.1775 27.3141 17.554 27.482 16.7866C27.6499 16.1151 27.7458 15.3477 27.8417 14.4844C27.9137 13.6211 27.9616 12.9017 27.9856 12.3501C27.9856 11.7986 28.0096 11.0072 28.0096 10C28.0096 8.99281 28.0096 8.22542 27.9856 7.64988ZM19.5444 10.8393L11.5348 15.8513C11.3909 15.9472 11.223 16.0192 11.0072 16.0192C10.8393 16.0192 10.6954 15.9712 10.5276 15.8993C10.1918 15.7074 10 15.4197 10 15.012V5.01199C10 4.60432 10.1679 4.31655 10.5276 4.1247C10.8873 3.93285 11.223 3.95683 11.5348 4.14868L15.5396 6.65468L19.5444 9.16067C19.8561 9.32854 20.024 9.61631 20.024 10C20 10.3837 19.8561 10.6715 19.5444 10.8393Z" fill="black"/>
              </g>
              <defs>
              <clipPath id="clip0">
              <rect width="28.0096" height="20" fill="white"/>
              </clipPath>
              </defs>
            </svg>
          </div>
        </div>
      </footer>
    </div>
  )
}
